const jwt = require("jsonwebtoken");


// User define string data taht will be used to create our JSOn web Token
// Used in the algorithm for encrupting our data which it diffult todeccode
const secret = "CourseBookingAPI";

// JSON WEN TOKENS


module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}


	//Generate json wen token using JWT that we decalre above
	return jwt.sign(data, secret, {});
	// return jwt.sign(data, secret, {expiresIn: "3d"});
}


// Token verification

module.exports.verify = (req, res, next) => {

	// Token is retrive from the request header
	// This can be provided in postman under Authorization > Token
	let token = req.headers.authorization;

	// Recieved and is not undfefined
	if(typeof token !== "undefined") {
		// console.log(token);

		// Slice method takes only the token from the information sent via thre request header
		// Token sent is a type of Bearer token which when recieved contains the word "Bearer" prefix and only obta
		// This removes "Bearer " prefix and only obtains the token
		token = token.slice(7, token.length)


		// Validate the token using the verify
		return jwt.verify(token, secret, (err, data) => {


			if(err) {
				console.log(err);
				return res.send({auth: "failed"})
			} else {

				// Allows application to proceed with the next middle ware function/callback function in the route
				// The verify method will nne used ion the middle ware in the route to verify token before proceeding to the functions
				next();
			}
		})
	} else {
		return res.send({auth: "failed"})
	}

}


// Token decryptiom

module.exports.decode = (token) => {


	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null;
			} else {


				// the devode method is used to obtain the info from the JWT
				// The "{complete: true}" options allows us to retirn additional information from the JWT

				// Returns an object with acces to the "payload" property wichich contains user information stored when the token was generated..
				// The payload contains information provided in the "createAccessToken" method defined above
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		return null
	}


}