const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");
// const jwt = require("jsonwebtoken");
// Route for checking the email exist in database
// Invokes the checkEmailExists function  from  the controller file

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Register USER
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Login user retrieve Route for user details single profile this use in login my login page
// "authi.verify" acts as a middle ware to ensure that the user is logged in before then can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// uses decode method defined in the auth.js file toretrieve user information from the token passing the "token" from the request headder
	const userData = auth.decode(req.headers.authorization);


	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
	userController.getProfile({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
})

// Route to addCart user

router.post("/addCart", auth.verify, (req, res) => {
	let data = {
		userData: auth.decode(req.headers.authorization),
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		productId: req.body.productId,
		name: req.body.name,
		price: req.body.price,
		quantity: req.body.quantity
	}
	userController.addCart(data).then(resultFromController => res.send(resultFromController));
});

// router.post("/addCart", auth.verify, (req, res) => {
// 	// let myAuth = auth.decode(req.headers.authorization);

// 	// let data = {
// 	// 	userId: myAuth.id,
// 	// 	isAdmin: myAuth.isAdmin,
// 	// 	courseId: req.body.courseId
// 	// }

// 	let data = {
// 		userData: auth.decode(req.headers.authorization),
// 		productId: req.body.productId
// 	}

// 	// this code is for admin authrorization
// 	if (data.isAdmin !== true) {
// 		userController.addCart(data).then(resultFromController => res.send(resultFromController))
// 	} else {
// 		res.send(false);
// 	}

// 	// userController.addCart(data).then(resultFromController => res.send(resultFromController))
// })



// Delete  single user
router.delete("/:userId", auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin

	if (admin == true) {
		userController.deleteUser(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})


// Get all users
router.get("/allUsers", (req, res) => {
	userController.retrieveUsers().then(resultFromController => res.send(resultFromController));
	// const data = {
	// 	userId: req.body,
	// 	isAdmin: auth.decode(req.headers.authorization).isAdmin
	// }

	// if (data.isAdmin == true) {
	// 	userController.retrieveUsers(data).then(resultFromController => res.send(resultFromController));
	// } else {
	// 	res.send(false);
	// }
})


// set As admin 
router.put("/setAsAdmin/:userId", auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		userController.setAdmin(req.params, data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
});

router.put("/setAsUser/:userId", auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		userController.setAsUser(req.params, data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
});

module.exports = router;

