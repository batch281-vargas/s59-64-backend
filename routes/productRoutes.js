const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for create a product

router.post("/addNewProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})


// Route for retrieving all the all product
router.get("/all", (req, res) => {
	productController.retrieveProducts().then(resultFromController => res.send(resultFromController));
})

// Route for regreiveing all the product that isActive
// router.get("/", (req, res) => {
// 	productController.getAllActive().then(resultFromController => res.send(resultFromController));
// })

// // Route for retrieving specific product
router.get("/:productId", (req, res) => {
	// console.log(req.params.productId);
	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Edit/Update Product 
router.put("/:productId/edit", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archived 
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})

// activate 
router.put("/:productId/activate", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		productController.activateProduct(req.params, data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})

// Delete Product
router.delete("/:productId/", auth.verify, (req, res) => {
	const admin = auth.decode(req.headers.authorization).isAdmin

	if (admin == true) {
		productController.deleteProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false);
	}
})



module.exports = router;