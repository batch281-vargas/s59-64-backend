const User = require("../models/User");
const Course = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		// Find method to returns record if a match is found
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// User registration
// HASHING BCRYPT 
// npm install bycrpyt


module.exports.registerUser = async (reqBody) => {
	// let newUser = new User({
	// 	firstName: reqBody.firstName,
	// 	lastName: reqBody.lastName,
	// 	email: reqBody.email,
	// 	mobileNo: reqBody.mobileNo,
	// 	password: bcrypt.hashSync(reqBody.password, 10)
	// })

	// return newUser.save().then((user, err) => {
	// 	if(err) {
	// 		return false;
	// 	} else {
	// 		return true;
	// 	}
	// })

	try {
		const existingUser = await User.findOne({ email: reqBody.email });
		if (existingUser) {
			return { success: false, message: 'Email already exists' };
		}

		// Create a new user
		const newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password, 10)
		});

		// Save the new user
		const savedUser = await newUser.save();
		return { success: true, message: 'Registration successful' };
	} catch (error) {
		console.error('Error registering user:', error);
		return { success: false, message: 'Error registering user' };
	}
}


// USER LOGIIN AUTHENTICATION

module.exports.loginUser = async (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {

		// User does not exists

		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false;
			}
		}
	})

	// const { email, password } = reqBody;


	// try {
	// 	if (!email || !password) {
	// 		return { error: `All fields must be filled` };
	// 	}
	// 	const user = await User.findOne({ email: reqBody.email });

	// 	if (user == null || user == undefined) {
	// 		return { error: `Invalid Credentials.` }
	// 	}

	// 	if (!user) {
	// 		return { error: `Incorrect email!` };
	// 	} else {

	// 		const match = bcrypt.compareSync(reqBody.password, result.password);

	// 		if (!match) {
	// 			return { error: `Incorrect Password` };
	// 		} else {
	// 			return {
	// 				access: auth.createAccessToken(user)
	// 			}
	// 		}
	// 	}
	// } catch (error) {
	// 	console.error(error);
	// 	return { error: `Invalid Credentials` };
	// }

}

// LOGIN / SINGLE USER DETAILS
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";

		return result;
	})
	// 	return User.findById(data.userId).then((result,err) => {
	// 	if(err) {
	// 		console.log(err);
	// 		return false;
	// 	} else {
	// 		return result;
	// 	}
	// })
}


// Retrieve all users by admin
module.exports.retrieveUsers = () => {
	return User.find({}).then(result => {
		result.password = "";
		return result;
	})
}

// Add product user to a class
// Async await will be used in adding the user in product

// module.exports.addCart = async (data) => {
// 	try {
// 		let user = await User.findById(data.userData.id);
// 		user.orderProducts.push({ productId: data.productId });
// 		await user.save();

// 		let product = await Product.findById(data.productId);
// 		product.orderedUsers.push({ userId: data.userData.id });
// 		await product.save();

// 		return true;
// 	} catch (error) {
// 		console.error(error);
// 		return false;
// 	}
// };


module.exports.addCart = async (data) => {

	let isUserUpdated = await User.findById(data.userData.id).then(user => {

		user.orderProducts.push(
			{
				productId: data.productId,
				name: data.name,
				price: data.price,
				quantity: data.quantity
			}
		)


		return user.save().then((user, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
	})


	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.orderedUsers.push(
			{
				userId: data.userData.id,
				firstName: data.userData.firstName,
				lastName: data.userData.lastName,
				email: data.userData.email
			}

		);

		return product.save().then((product, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		});
	});


	if (isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}

}

// set as admin
module.exports.setAdmin = (reqParams, data) => {
	let setAsAdmin = {
		...data.user, // Spread the properties of the user object
		isAdmin: true, // Set isAdmin to true or any other value to mark it as admin
	};

	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin)
		.then((admin) => {
			if (!admin) {
				return false;
			} else {
				return true;
			}
		})
		.catch((error) => {
			console.error(error);
			return false;
		});
};

// set as user
module.exports.setAsUser = (reqParams, data) => {
	let setAsAdmin = {
		...data.user, // Spread the properties of the user object
		isAdmin: false, // Set isAdmin to false or any other value to mark it as user
	};

	return User.findByIdAndUpdate(reqParams.userId, setAsAdmin)
		.then((admin) => {
			if (!admin) {
				return false;
			} else {
				return true;
			}
		})
		.catch((error) => {
			console.error(error);
			return false;
		});
};



// Delete User
module.exports.deleteUser = (reqParams) => {
	return User.deleteOne({ _id: reqParams.userId })
		.then((result) => {
			if (result.deletedCount === 1) {
				return { success: true };
			} else {
				return { success: false, message: 'User not found' };
			}
		})
		.catch((error) => {
			console.error(error);
			return { success: false, message: 'Failed to delete the user' };
		});
};