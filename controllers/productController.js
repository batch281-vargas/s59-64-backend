const Product = require("../models/Product");


// Create product controller
module.exports.addProduct = (data) => {

	let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
		image: data.product.image
	});

	return newProduct.save().then((product, err) => {
		if (product) {
			return true;
		} else {
			console.error("Error adding course:", err);
			return false;
		}
	})
}

// Comment
// // Retrieve all product
module.exports.retrieveProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


// // Route for retrieving a specific Product

module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId)
		.then((result) => {
			if (result) {
				return result;
			} else {
				throw new Error("Product not found");
			}
		})
		.catch((error) => {
			throw error;
		});
};

// Update a product
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product) => {
		if (!product) {
			return false;
		} else {
			return true;
		}
	})
		.catch((error) => {
			console.error(error);
			return false;
		});
}



// Archive
module.exports.archiveProduct = (reqParams, data) => {
	let archivedProduct = {
		...data.product, // Spread the properties of the product object
		isActive: false, // Set isActive to false or any other value to mark it as archived
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
		.then((product) => {
			if (!product) {
				return false;
			} else {
				return true;
			}
		})
		.catch((error) => {
			console.error(error);
			return false;
		});
};

// Activate
module.exports.activateProduct = (reqParams, data) => {
	let archivedProduct = {
		...data.product, // Spread the properties of the product object
		isActive: true, // Set isActive to false or any other value to mark it as archived
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
		.then((product) => {
			if (!product) {
				return false;
			} else {
				return true;
			}
		})
		.catch((error) => {
			console.error(error);
			return false;
		});
};


// Delete Product
module.exports.deleteProduct = (reqParams) => {
	return Product.deleteOne({ _id: reqParams.productId })
		.then((result) => {
			if (result.deletedCount === 1) {
				return { success: true };
			} else {
				return { success: false, message: 'Product not found' };
			}
		})
		.catch((error) => {
			console.error(error);
			return { success: false, message: 'Failed to delete the product' };
		});
};

// module.exports.deleteProduct = (reqParams) => {
// 	return Product.delete(reqParams.productId)
// 		.then((result) => {
// 			if (result.deletedCount === 1) {
// 				return { success: true };
// 			} else {
// 				return { success: false, message: 'Product not found' };
// 			}
// 		})
// 		.catch((error) => {
// 			console.error(error);
// 			return { success: false, message: 'Failed to delete the product' };
// 		});
// };
