const express = require("express");
const mongoose = require("mongoose");

// Cors allows us all resources to access our backend application
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

// Allows us to use express.js to use to our backend codes
const app = express();

// Connect to our MongoDB || SERVER API
mongoose.connect("mongodb+srv://nielvargas96:MbQPGFGeZkjPBnXt@wdc028-course-booking.wixug5i.mongodb.net/capstone_3_API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."))
db.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// Cors allows us all resources to access our backend application
app.use(cors());

// add json middle ware allows us to parse incoming request / allow us to send request to our body to our routes
// allows as to parse json data
app.use(express.json());


// middle ware parses form data that going to submit via POST request
// this parse forms data coming from our forms
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online... on port ${process.env.PORT || 4000}`);
});

