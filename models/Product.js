const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	image: {
		type: String,
		required: [true, "Image is required"]
	},
	quantity: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orderedUsers: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			firstName: {
				type: String
			},
			lastName: {
				type: String
			},
			email: {
				type: String
			},
			orderedDate: {
				type: Date,
				default: new Date()
			}
		}
	]
})


module.exports = mongoose.model("Product", productSchema);	